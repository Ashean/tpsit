/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vigenereendecryption;

/**
 *
 * @author hettige.abeysinghe
 */
public class Encryption {
	public String worm;
	String alfabeto;
	
	Encryption(){
		this.worm="";
		this.alfabeto="";
	}
	
	Encryption(String worm,String alfabeto){
		this.worm=worm;
		this.alfabeto = alfabeto;
	}

	public String getAlfabeto() {
		return alfabeto;
	}

	public void setAlfabeto(String alfabeto) {
		this.alfabeto = alfabeto;
	}

	public String getWorm() {
		return worm;
	}

	public void setWorm(String worm) {
		this.worm = worm;
	}
	
	public String encrypt (String msg){
		String encryptedMsg="";
		for(int i = 0; i< msg.length() -1;i++){
			char msgCurrentChar = msg.charAt(i);
			char keyCurrentChar = worm.charAt(i%worm.length());
			encryptedMsg += alfabeto.charAt(((alfabeto.indexOf(msgCurrentChar))+(alfabeto.indexOf(keyCurrentChar)))%alfabeto.length());
			
		}
		return encryptedMsg;
	}
        
        public String dencrypt (String msg){
		String decryptedMsg="";
		for(int i = 0; i< msg.length() -1;i++){
			char msgCurrentChar = msg.charAt(i);
			char keyCurrentChar = worm.charAt(i%worm.length());
			decryptedMsg += alfabeto.charAt(Math.abs((alfabeto.indexOf(msgCurrentChar))-(alfabeto.indexOf(keyCurrentChar)))%alfabeto.length());
			
		}
		return decryptedMsg;
	}
}
