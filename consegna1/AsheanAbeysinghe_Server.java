package server1;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 *
 * @author hettige.abeysinghe
 */
public class Server {

	private ServerSocket server_socket;
	private Socket socket;
	private ObjectOutputStream output_stream;
	private ObjectInputStream input_stream;
        private String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ,.:!?"; // 58
        private String worm = "ovidio";


	public Server() throws IOException, ClassNotFoundException {
		try // creo il server socket in ascolto sulla porta 30000 con massimo 10 client in coda
		{
			server_socket = new ServerSocket(30000, 10);
			
			listen();
			createStreams();
			initProcessing();
		} finally {
			close();
		}
	}

	private void listen() throws IOException {
		System.out.println("SERVER -> IN ATTESA DI CONNESSIONI ...");
		//server_socket.setSoTimeout(3000);
		socket = server_socket.accept();
		showMessage("SERVER -> CONNESSO CON " + socket.getInetAddress() + " ALLA PORTA REMOTA " + socket.getPort() + " ALLA PORTA LOCALE " + socket.getLocalPort());
	}

	private void createStreams() throws IOException {
		output_stream = new ObjectOutputStream(socket.getOutputStream());
		output_stream.flush();

		input_stream = new ObjectInputStream(socket.getInputStream());
		showMessage("SERVER -> STREAM CREATI");
	}

	private void initProcessing() throws IOException, ClassNotFoundException {
		String client_msg = "";
		String de_client_msg="";
		sendDataToClient("SERVER -> Ciao digita BYE per terminare ...");
		do {
			client_msg = (String) input_stream.readObject();
			showMessage("SERVER -> MESSAGGIO RICEVUTO :" + client_msg);
			de_client_msg = decriptaggio(alfabeto, worm,client_msg );
			showMessage("SERVER -> MESSAGGIO DECIFRATO RICEVUTO :" + de_client_msg);
			sendDataToClient(client_msg);
		} while (!client_msg.trim().equals("BYE"));
	}

	private void sendDataToClient(String msg) throws IOException {
		output_stream.writeObject(msg);
		output_stream.flush();
	}

	private void close() throws IOException {
		showMessage("SERVER -> CHIUSURA CONNESSIONE SOCKET");

		if (output_stream != null && input_stream != null && socket != null) {
			output_stream.close();
			input_stream.close();
			socket.close();
		}
	}

	private void showMessage(String string) {
		System.out.println(string);
	}
        
        
        public static String criptaggio(String alfabeto, String worm, String rispStr) {
		
    
		
		String parolaCriptata = new String();

		for (int i = 0, j = 0; i < rispStr.length(); i++, j++) {
			if(j == worm.length()){
				j = 0;
			}
			
			int v = (alfabeto.indexOf(rispStr.charAt(i))) + (alfabeto.indexOf(worm.charAt(j)));
			while (v > alfabeto.length()-1) {
				v = v - (alfabeto.length());
			}
			
			parolaCriptata += alfabeto.charAt(v);
			//System.out.println("Da " + parolaChar[i] + " a " + parolaCriptata[i]);
		}
		return (parolaCriptata);
	}

	public static String decriptaggio(String alfabeto, String worm,String rispStr) {

		String parolaDecriptata = new String();

		for (int i = 0, j = 0; i < rispStr.length(); i++, j++) {
			if(j == worm.length()){
				j = 0;
			}
                        
			int v = (alfabeto.indexOf(rispStr.charAt(i))) - (alfabeto.indexOf(worm.charAt(j)));
			while (v < 0) {
				v = (alfabeto.length()) + v;
			}
			
			parolaDecriptata += alfabeto.charAt(v);
			//System.out.println("Da " + parolaChar[i] + " a " + parolaDecriptata[i]);
		}
                return (parolaDecriptata);
	}

	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		new Server();
	}

}
