package client1;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 *
 * @author matteo.derossi
 */
public class Client {

	private ServerSocket server_socket;
	private Socket socket;
	private ObjectOutputStream output_stream;
	private ObjectInputStream input_stream;
        private String alfabeto = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ,.:!?/"; // 58
        private String worm = "ovidio";

	public Client() throws IOException, ClassNotFoundException {
		try {
			attemptToConnect();
			createStreams();
			initProcessing();
		} finally {
			close();
		}
	}

	private void attemptToConnect() throws IOException {
		showMessage("CLIENT -> CONNESSIONE VERSO UN SERVER...");
		socket = new Socket("localhost", 30000); // connessione verso il server in ascolto
		showMessage("CLIENT -> CONNESSIONE AVVENUTA VERSO "
				+ socket.getInetAddress() + " ALLA PORTA REMOTA "
				+ socket.getPort() + " E ALLA PORTA LOCALE " + socket.getLocalPort());
	}

	private void createStreams() throws IOException {
		output_stream = new ObjectOutputStream(socket.getOutputStream());
		output_stream.flush();

		input_stream = new ObjectInputStream(socket.getInputStream());
		showMessage("CLIENT -> STREAM CREATI");
	}

	private void initProcessing() throws IOException, ClassNotFoundException {
		// messaggio benvenuto del server
		String server_msg = (String) input_stream.readObject();
		String de_server_msg ;
		showMessage(server_msg);
		String msg_to_send="";
		String cipher_msg="";
		
		

		do // interazione col server
		{
			// sendDataToServer(readFromInput());
			System.out.println("");
			showMessage("CLIENT -> SCRIVI IL LA TUA DATA DI NASCITA NEL FORMATO gg/mm/aaaa ");
			msg_to_send=readFromInput();
			if(correctFormat(msg_to_send)){
				showMessage("CLIENT -> DATA NEL FORMATO CORRETTO, INVIO DATI AL SERVER " );
			//Creo la chiave utilizzando come WORM :"CS" e il messaggio in codice			
			cipher_msg=criptaggio(alfabeto,worm,msg_to_send);

			output_stream.writeObject(cipher_msg);
			output_stream.flush();
			server_msg = (String) input_stream.readObject();
			
			System.out.println("");
			de_server_msg= decriptaggio(alfabeto,worm,server_msg);
			showMessage("CLIENT -> LA TUA ETA' E'" + de_server_msg);
			}else{
				showMessage("IL DATO INSERITO NON E' NEL CORRETTO FROMATO, PERFAVORE RIPROVARE");
			}
			
			
			System.out.println("");
		} while (!server_msg.contains("BYE"));
	}

	private void showMessage(String string) {
		System.out.println(string);
	}

	private void close() throws IOException {
		showMessage("CLIENT -> CHIUSURA CONNESSIONE SOCKET");

		if (output_stream != null && input_stream != null && socket != null) {
			output_stream.close();
			input_stream.close();
			socket.close();
		}
	}

	/*private void sendDataToServer(String msg) throws IOException {
		output_stream.writeObject(msg);
		output_stream.flush();
	}*/

	private String readFromInput() {
		Scanner in = new Scanner(System.in);
		return in.nextLine();
	}
	
	
// Java code to implement Vigenere Cipher 
  
   public static String criptaggio(String alfabeto, String worm, String rispStr) {		
		String parolaCriptata = new String();
		for (int i = 0, j = 0; i < rispStr.length(); i++, j++) {
			if(j == worm.length()){
				j = 0;
			}
			
			int v = (alfabeto.indexOf(rispStr.charAt(i))) + (alfabeto.indexOf(worm.charAt(j)));
			while (v > alfabeto.length()-1) {
				v = v - (alfabeto.length());
			}
			
			parolaCriptata += alfabeto.charAt(v);
		}
		return parolaCriptata;
	}

	public static String decriptaggio(String alfabeto, String worm,String rispStr) {
		
		String parolaDecriptata = new String();

		for (int i = 0, j = 0; i < rispStr.length(); i++, j++) {
			if(j == worm.length()){
				j = 0;
			}
                        
			int v = (alfabeto.indexOf(rispStr.charAt(i))) - (alfabeto.indexOf(worm.charAt(j)));
			while (v < 0) {
				v = (alfabeto.length()) + v;
			}
			
			parolaDecriptata += alfabeto.charAt(v);
                }
                return  parolaDecriptata;
	}
	
	private boolean correctFormat(String data){
		
		boolean correct =  ((data.length() == 10)&&(data.charAt(2)=='/')&&(data.charAt(5)=='/'));
		
		return correct;
	}
 
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		new Client();
	}
}
