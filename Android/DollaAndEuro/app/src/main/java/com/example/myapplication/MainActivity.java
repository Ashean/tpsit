package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.view.*;
import android.widget.*;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button dollaToEuro = findViewById(R.id.button2);
        final Button euroToDollar = findViewById(R.id.button);

        final EditText inEuro = findViewById(R.id.editText2);
        final EditText inDollar = findViewById(R.id.editText1);


        dollaToEuro.setOnClickListener(new View.OnClickListener(){
            public void onClick (View w){
                double euro =0;
                double dollar = Double.parseDouble(inDollar.getText().toString());
                euro = dollar * 0.90;
                inEuro.setText(String.valueOf(euro));
            }
        });

        euroToDollar.setOnClickListener(new View.OnClickListener(){
            public void onClick (View w){
                double dollar =0;
                double euro = Double.parseDouble(inEuro.getText().toString());
                dollar = euro * 1.11;
                inDollar.setText(String.valueOf(dollar));
            }
        });
    }
}
