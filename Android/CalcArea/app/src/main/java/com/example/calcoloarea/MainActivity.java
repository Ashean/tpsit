package com.example.calcoloarea;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final double  pi = 3.14159;

        final EditText inRaggio = findViewById(R.id.in_raggio);
        final TextView resultArea = findViewById(R.id.lal_resultArea);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String str_raggio = inRaggio.getText().toString();
                int int_raggio = Integer.parseInt(str_raggio);
                double result = int_raggio * pi;
                resultArea.setText(String.valueOf(result));
            }
        });




    }
}
