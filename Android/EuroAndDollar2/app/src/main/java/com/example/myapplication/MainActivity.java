package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.view.*;
import android.widget.*;
import android.os.Bundle;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button euroToDollar = findViewById(R.id.button);

        final EditText inEuro = findViewById(R.id.editText2);
        final EditText inDollar = findViewById(R.id.editText1);

        euroToDollar.setOnClickListener(new View.OnClickListener(){
            public void onClick (View w){
                String euro =inEuro.getText().toString();
                String dollar = inDollar.getText().toString();
                if(euro.equals("")){
                    double Ddollar = Double.parseDouble(inDollar.getText().toString());
                    inEuro.setText(String.valueOf(Ddollar * 0.90));
                }else if (dollar.equals("")){
                    double Deuro = Double.parseDouble(inEuro.getText().toString());
                    inDollar.setText(String.valueOf(Deuro * 1.11));
                }

            }
        });
    }
}
