
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codificatore;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author ashea
 */
public class Crypto {

    public int key;

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    Crypto(int k) {
        this.key = k;
    }

    public int Crypto() {
        int k = ThreadLocalRandom.current().nextInt(0, 24 + 1);
        return k;
    }

    public String encryption(String data, int key) {
        String encryptedString = "";

        for (int i = 0; i <= data.length() - 1; i++) {
            int indexAscii = key + data.charAt(i);
            if ((indexAscii) < 122) {
                encryptedString += (char) indexAscii;
            } else {
                int currentKey = 97 + (indexAscii - 127);
                encryptedString += (char) (currentKey);
            }

        }

        return encryptedString;
    }

    public void saveKey() {
        String path = System.getProperty("user.dir");
     /*   File crunchifyFile = new File(path);
        if (!crunchifyFile.exists()) {
            try {
                File directory = new File(crunchifyFile.getParent());
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                crunchifyFile.createNewFile();
            } catch (IOException e) {
                System.out.println("Excepton Occured: " + e.toString());
            }
        }

        try {
            // Convenience class for writing character files
            FileWriter crunchifyWriter;
            crunchifyWriter = new FileWriter(crunchifyFile.getAbsoluteFile(), true);

            // Writes text to a character-output stream
            BufferedWriter bufferWriter = new BufferedWriter(crunchifyWriter);
            bufferWriter.write(key);
            bufferWriter.close();

            System.out.println("Key encryption is saved in" + path.toString() + "\n");
        } catch (IOException e) {
            System.out.println("Hmm.. Got an error while saving ket in a file " + e.toString());
        }*/
     
        try {

            // Java 11 , default StandardCharsets.UTF_8
            Files.write(Paths.get(path), key);

            // encoding
            // Files.writeString(Paths.get(path), content, StandardCharsets.US_ASCII);

            // extra options
            // Files.writeString(Paths.get(path), content, 
			//		StandardOpenOption.CREATE, StandardOpenOption.APPEND);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveKey(String path) {
        File crunchifyFile = new File(path);
        if (!crunchifyFile.exists()) {
            try {
                File directory = new File(crunchifyFile.getParent());
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                crunchifyFile.createNewFile();
            } catch (IOException e) {
                System.out.println("Excepton Occured: " + e.toString());
            }
        }

        try {
            // Convenience class for writing character files
            FileWriter crunchifyWriter;
            crunchifyWriter = new FileWriter(crunchifyFile.getAbsoluteFile(), true);

            // Writes text to a character-output stream
            BufferedWriter bufferWriter = new BufferedWriter(crunchifyWriter);
            bufferWriter.write(key);
            bufferWriter.close();

            System.out.println("Key encryption is saved in" + path.toString() + "\n");
        } catch (IOException e) {
            System.out.println("Hmm.. Got an error while saving ket in a file " + e.toString());
        }
    }
}
