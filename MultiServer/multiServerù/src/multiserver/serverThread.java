/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiserver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author hettige.abeysinghe
 */
public class serverThread {
	private Socket client = null;
	private String clientIp = null;
	private String name=null;

	public serverThread(Socket client)
	{
		this.client = client;
		clientIp = this.client.getInetAddress().getHostAddress();
		System.out.println("[" + clientIp + "] " + ">> Connessione in ingresso <<");
	}

	public void run()
	{
		ObjectOutputStream output_stream = null;
		ObjectInputStream input_stream = null;
		try
		{
			output_stream = new ObjectOutputStream(client.getOutputStream());
			output_stream.flush();

			input_stream = new ObjectInputStream(client.getInputStream());
			System.out.println("SERVER -> STREAM CREATI");
				
			String client_msg = "";
			output_stream.writeObject("SERVER -> DIMMI IL TUO NOME");
			/*client_msg = (String) input_stream.readObject();
			name = client_msg;*/
			output_stream.flush();

			do
			{
				client_msg = (String) input_stream.readObject();
				System.out.println( "CLIENT" +  "-> " + client_msg);
				output_stream.writeObject(client_msg);
				output_stream.flush();
			}
			while (!client_msg.trim().equals("BYE"));
		}
		catch(IOException ex)
		{
			
		}
		catch(ClassNotFoundException ec) 
		{
			
		}
		finally
		{
			try
			{
				if (output_stream != null) output_stream.close();
				if (input_stream  != null) input_stream.close();
				client.close();
			}
			catch(IOException ex)
			{
				ex.printStackTrace();
			}		

			System.out.println("SERVER -> CHIUSURA CONNESSIONE SOCKET [" + clientIp + "]");
		}
	}
}
