/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decodificatore;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author ashea
 */
public class DeCodificatore {

    /**
     * @param args the command line arguments
     */
 
  
  public static void main(String[] args) throws Exception 
  { 
    String fileName =  System.getProperty("user.dir") + "\\key.txt";
    String a = (readFileAsString(fileName).replace("\r\n", "")); 
    int key = Integer.valueOf(a);
    DeCrypto decrypto = new DeCrypto(key);
    String cryptedMsg = "aaaa";
      System.out.println(decrypto.decryption(cryptedMsg));
  } 
    
  
   public static String readFileAsString(String fileName)throws Exception 
  { 
    String data = ""; 
    data = new String(Files.readAllBytes(Paths.get(fileName))); 
    return data; 
  } 
}
