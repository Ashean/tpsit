package decodificatore;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ashea
 */
public class DeCrypto {

     int key;

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    DeCrypto(){
        
    }
    DeCrypto(int key) {
        this.key = key;
    }

    public String decryption(String encryptedMsg) {
        String decryptedMsg = "";

        for (int i = 0; i <= encryptedMsg.length() - 1; i++) {
            int indexAscii = encryptedMsg.charAt(i) - key;
            if ((indexAscii) >= 97) {
                decryptedMsg += (char) indexAscii;
            } else {
                int currentKey = 122 - (96 - indexAscii);
                decryptedMsg += (char) (currentKey);
            }
        }
        return decryptedMsg;

    }
}
